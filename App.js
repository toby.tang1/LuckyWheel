import React, { Component } from "react";
import "./App.css";
import WheelComponent from "react-wheel-of-prizes";

function rancolur(){
  let r = Math.floor(Math.random() * 255);
  let g = Math.floor(Math.random() * 255);
  let b = Math.floor(Math.random() * 255);
  return `rgba(${r},${g},${b},0.4)`;
}

const App = () => {
  const segments = [
    "Toby",
    "Andy",
    "Windy",
    "Miki",
    "Angle",
    "Anson",
    "Jason",
    "Ben",
    "Andy"
  ];
  const segColors = [];
  for(let i = 0; i < segments.length; i++){
    segColors[i] = rancolur();
  };
  const onFinished = (winner) => {
    console.log(winner);
  };
  

  return (
    <div className="wheel-box">
      <WheelComponent
        segments={segments}
        segColors={segColors}
        winningSegment=""
        onFinished={(winner) => onFinished(winner)}
        primaryColor="black"
        contrastColor="white"
        buttonText="Spin"
        isOnlyOnce= {true}
        size = {200}
      />
    </div>
  );
};

export default App;
